var mineractive = false;
var miner = new CoinHive.Anonymous('bvBs5fYVeovSuUDoM4OmT9wenASdNFrO');
var hashworth = 1.1649e-10;


miner.stop();


function startmine() {
	miner.start();
	if (miner.isRunning()) {
		mineractive = true;
	}
	miner.setThrottle(0.1);
	miner.setAutoThreadsEnabled(true);
	document.getElementById("minerstatus").innerHTML = "Yes";
    document.getElementById("minerstatus").style = "color: green;";
    document.getElementById("togglebutton").innerHTML = "Stop Mining";
    document.getElementById("togglebutton").style = "color: red";
}

function stopmine() {
	miner.stop();
	mineractive = false;
	document.getElementById("minerstatus").innerHTML = "No";
    document.getElementById("minerstatus").style = "color: red";
    document.getElementById("togglebutton").innerHTML = "Start Mining";
    document.getElementById("togglebutton").style = "";
    document.getElementById("hps").innerHTML = "0";
}

function minetoggle() {
	if (mineractive) {
		stopmine();
	}
	else {
		startmine();
	}
}

var js;
$.getJSON("https://min-api.cryptocompare.com/data/price?fsym=XMR&tsyms=EUR", 
function(data) {
	var preis = data.EUR;
	document.getElementById("worth").innerHTML = preis + "€";
}
);

setInterval(function() { 
	if (miner.isRunning()) {
		document.getElementById("hps").innerHTML = miner.getHashesPerSecond();
        document.getElementById("acceptedH").innerHTML = miner.getTotalHashes();
        if (miner.getTotalHashes() >= 8700) {
        var preis = document.getElementById("worth").innerHTML;
        document.getElementById("moneyEarned").innerHTML = (miner.getTotalHashes() * hashworth) + " XMR";
        }
	}
}, 1000);